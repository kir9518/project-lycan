#!/usr/bin/env python

# IMEI editor for Qualcomm phones with tunning partition

# Created by Luxferre in 2021
# Released into public domain

import tarfile
from io import BytesIO

# define the constants

VERSION = '0.1'
IMEI1PATH = './nvm/num/550'
IMEI2PATH = './nvm/context1/550'

# nibble inversion helper function

def nibble_invert(bytestr):
    r = b''
    for b in bytestr:
        c = (b >> 4) | ((b & 15) << 4)
        r += bytes([c])
    return r

# encode IMEI from string into Qualcomm format

def encode_imei(imeistr):
    return nibble_invert(bytes.fromhex('80a' + imeistr.strip()))

# decode IMEI from Qualcomm format into string

def decode_imei(bytestr):
    return bytes.hex(nibble_invert(bytestr))[3:]

# read both IMEIs in the Qualcomm format

def read_imei(fh):
    br1 = fh.extractfile(IMEI1PATH)
    imei1 = br1.read(9)
    br2 = fh.extractfile(IMEI2PATH)
    imei2 = br2.read(9)
    return imei1, imei2

# write both IMEIs in the Qualcomm format

def write_imei(tar_read, tar_write, imei1, imei2):
    for info in tar_read.getmembers():
        if info.name == IMEI1PATH:
            linfo = tarfile.TarInfo(name=IMEI1PATH)
            linfo.size = len(imei1)
            f = BytesIO(imei1)
            tar_write.addfile(linfo, f)
        elif info.name == IMEI2PATH:
            linfo = tarfile.TarInfo(name=IMEI2PATH)
            linfo.size = len(imei2)
            f = BytesIO(imei2)
            tar_write.addfile(linfo, f)
        else:
            extracted = tar_read.extractfile(info)
            if not extracted:
                continue
            tar_write.addfile(info, extracted)

# main code

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Lycan TUN v%s: IMEI editor for Qualcomm phones with tunning partition' % VERSION, epilog='(c) Luxferre 2021 --- No rights reserved <https://unlicense.org>')
    parser.add_argument('image', help='tunning partition image file')
    parser.add_argument('-i1','--imei1', type=str, default=None, help='IMEI 1 (can be empty, will read the current one only)')
    parser.add_argument('-i2','--imei2', type=str, default=None, help='IMEI 2 (can be empty, will read the current one only)')
    parser.add_argument('-v','--version', action='version', version='Lycan TUN %s' % VERSION, help='Show version and exit')
    args = parser.parse_args()
    if tarfile.is_tarfile(args.image) == False:
        print('Tunning partition image is not in a compatible format, exiting!')
        exit(1)
    with tarfile.open(args.image, 'r:') as imagefile_read:
        current_imei1, current_imei2 = read_imei(imagefile_read)
        print('Current IMEI 1: %s' % decode_imei(current_imei1))
        print('Current IMEI 2: %s' % decode_imei(current_imei2))
        if args.imei1 or args.imei2:
            active_imei1 = current_imei1
            active_imei2 = current_imei2
            if args.imei1:
                print('New IMEI 1: ' + args.imei1)
                active_imei1 = encode_imei(args.imei1)
            if args.imei2:
                print('New IMEI 2: ' + args.imei2)
                active_imei2 = encode_imei(args.imei2)
            target_img = args.image + '.modimg'
            with tarfile.open(target_img, 'w:') as imagefile_write:
                print('Writing IMEI numbers...')
                write_imei(imagefile_read, imagefile_write, active_imei1, active_imei2)
                print('Done! Modified image written to %s' % target_img)
            imagefile_write.close()
    imagefile_read.close()
