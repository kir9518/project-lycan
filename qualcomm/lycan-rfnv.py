#!/usr/bin/env python

# IMEI editor for Qualcomm phones with rf_nv partition

# Created by Luxferre in 2021
# Released into public domain

from struct import pack
from binascii import crc32

# define the constants

VERSION = '0.2'
SIGVAL = b'IEMI' # little-endian "IMEI"
SIGPOS = 0x80
SIGVAL2 = b'\x92\x8a\x9a\x92' # little-endian "IMEI" where every character is bitshifted left by 1 bit
SIGPOS2 = SIGPOS + 0x80
IMEI1POS = SIGPOS + 8
CRC1POS = SIGPOS + 0x14
IMEI2POS = SIGPOS2 + 8
CRC2POS = SIGPOS2 + 0x14

# nibble inversion helper function

def nibble_invert(bytestr):
    r = b''
    for b in bytestr:
        c = (b >> 4) | ((b & 15) << 4)
        r += bytes([c])
    return r

# encode IMEI from string into Qualcomm format

def encode_imei(imeistr):
    return nibble_invert(bytes.fromhex('80a' + imeistr.strip()))

# decode IMEI from Qualcomm format into string

def decode_imei(bytestr):
    return bytes.hex(nibble_invert(bytestr))[3:]

# read both IMEIs in the Qualcomm format

def read_imei(fh):
    fh.seek(IMEI1POS)
    imei1 = fh.read(9)
    fh.seek(IMEI2POS)
    imei2 = fh.read(9)
    return imei1, imei2

# calculate CRCs from IMEI1 and IMEI2 in the Qualcomm format

def calc_crc(imei1, imei2):
    crc1 = pack('<L', crc32(imei1))
    crc2 = pack('<L', crc32(imei1 + imei2))
    return crc1, crc2

# write both IMEIs in the Qualcomm format and their CRCs

def write_imei(fh, imei1, imei2):
    crc1, crc2 = calc_crc(imei1, imei2)
    fh.seek(IMEI1POS)
    fh.write(imei1)
    fh.seek(CRC1POS)
    fh.write(crc1)
    fh.seek(IMEI2POS)
    fh.write(imei2)
    fh.seek(CRC2POS)
    fh.write(crc2)

# check the signature position

def check_valid_sig(fh):
    fh.seek(SIGPOS)
    val1 = fh.read(4)
    fh.seek(SIGPOS2)
    val2 = fh.read(4)
    return (val1 == SIGVAL and val2 == SIGVAL2)

# main code

if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Lycan RFNV v%s: IMEI editor for Qualcomm phones with rf_nv partition' % VERSION, epilog='(c) Luxferre 2021 --- No rights reserved <https://unlicense.org>')
    parser.add_argument('image', help='rf_nv partition image file')
    parser.add_argument('-i1','--imei1', type=str, default=None, help='IMEI 1 (can be empty, will read the current one only)')
    parser.add_argument('-i2','--imei2', type=str, default=None, help='IMEI 2 (can be empty, will read the current one only)')
    parser.add_argument('-v','--version', action='version', version='Lycan RFNV %s' % VERSION, help='Show version and exit')
    args = parser.parse_args()
    with open(args.image, 'r+b') as imagefile:
        if check_valid_sig(imagefile) == False:
            imagefile.close()
            print('Image file %s does not look like a valid rf_nv image, closing!' % args.image)
            exit(1)
        current_imei1, current_imei2 = read_imei(imagefile)
        print('Current IMEI 1: %s' % decode_imei(current_imei1))
        print('Current IMEI 2: %s' % decode_imei(current_imei2))
        if args.imei1 or args.imei2:
            active_imei1 = current_imei1
            active_imei2 = current_imei2
            if args.imei1:
                print('New IMEI 1: ' + args.imei1)
                active_imei1 = encode_imei(args.imei1)
            if args.imei2:
                print('New IMEI 2: ' + args.imei2)
                active_imei2 = encode_imei(args.imei2)
            print('Writing IMEI numbers and checksums...')
            write_imei(imagefile, active_imei1, active_imei2)
            print('Done!')
    imagefile.close()
